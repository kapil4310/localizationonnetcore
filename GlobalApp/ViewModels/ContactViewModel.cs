﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GlobalApp.ViewModels
{
    public class ContactViewModel
    {
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage ="The Email is not a valid email address")]
        [Display(Name = "Your Email")]
        public string Email { get; set; }
    }
}
