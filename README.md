#Project Title

Localization on the web using ASP.NET Core

#Project Description

Created a simple MVC Template without authentication.
Used the resources folder to put all the supported languages.
Did translation in three places which are Model, View and Controller.
Provide the cultures that I want to support in the application and 
default culture will be english used

localized in HomeController using IStringLocalizer

In View(About.cshtml)
inject IViewLocaizer Object from MVC.localization

Built resources language file according to the MVC in resources folder
In Model
Create a ContactViewModel with one property which is email.

Create a partial view to provide the language supported using a dropdown list
along with the save button .